// oems.cpp
// Odd-Even Merge Sort, PRL project 1, 20.3.2022
// Author: Matěj Kudera, xkuder04, FIT
// Main file with implementation of Odd-Even Merge Sort

// ############### Preprocesor directives ####################

#define LOW_TAG 1
#define HIGH_TAG 2
#define DEFAULT_TAG 3

// ############### Includes #################
#include <mpi.h>

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

// ############# Constants ################

const string file_name = "numbers";
const int input_size = 8;
const int sort_num_sender[input_size][2] = {
	{10, LOW_TAG}, 
	{16, LOW_TAG}, 
	{16, HIGH_TAG}, 
	{17, LOW_TAG}, 
	{17, HIGH_TAG},
	{18, LOW_TAG}, 
	{18, HIGH_TAG}, 
	{13, HIGH_TAG}
};
const int send_map[19][2] = {
	{4,5},
	{4,5},
	{6,7},
	{6,7},
	{10,8},
	{8,13},
	{10,9},
	{9,13},
	{12,11},
	{12,11},
	{0,14},
	{14,18},
	{16,15},
	{15,0},
	{16,17},
	{17,18},
	{0,0},
	{0,0},
	{0,0}
};
// Without first four processes which read from file
const int recv_map[15][2] = {
	{0,1},
	{0,1},
	{2,3},
	{2,3},
	{4,5},
	{6,7},
	{4,6},
	{8,9},
	{8,9},
	{5,7},
	{10,11},
	{12,13},
	{12,14},
	{14,15},
	{11,15}
};

// ############# Functions ################

// Read input file and print numbers in specified format
int print_input_nums(string file)
{
	char nums_char[input_size];
	unsigned int nums_int[input_size];
	string print_str = "";
	
	// Open file
	ifstream input_file (file_name, ios::in | ios::binary);		
	if (!input_file)
	{
		return -1;		
	}

	// Read to array
	input_file.read(nums_char, input_size);

	// Check if all was read corectly
	if (!input_file)
	{
		return -2;
	}	

	// Close file
	input_file.close();

	// Convert chars to unsigned ints
	for (int i = 0; i < input_size; i++)
	{
		nums_int[i] = (unsigned char)nums_char[i];
	}		

	// Print numbers
	for (int i = 0; i < input_size; i++)
	{
		print_str.append(to_string(nums_int[i]));	
		print_str.append(" ");				
	}

	// Delete last space
	print_str.pop_back();

	cout << print_str << endl;
	
	return 0;
}

// Read number in input file by given position
int read_num (string file, int pos)
{
	char nums_char[input_size];
	unsigned int nums_int[input_size];
	
	// Open file
	ifstream input_file (file_name, ios::in | ios::binary);		
	if (!input_file)
	{
		return -1;		
	}

	// Read to array
	input_file.read(nums_char, input_size);

	// Check if all was read corectly
	if (!input_file)
	{
		return -2;
	}	

	// Close file
	input_file.close();

	// Convert chars to unsigned ints
	for (int i = 0; i < input_size; i++)
	{
		nums_int[i] = (unsigned char)nums_char[i];
	}		
	
	// Return desired number
	return nums_int[pos];
}

// ############### Main ####################
int main(int argc, char **argv)
{
	// Start proceses
	MPI_Init(&argc, &argv);

	// Create variables for each process
	int rank;
	int num_proc;
	MPI_Status status;

	// Get number of each process and total number of processes
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

	// Print given sequence of unsorted numbers in process 0
	if (rank == 0)
	{
		int result = print_input_nums(file_name);
		if (result == -1)
		{
			cerr << "Could not open file" << endl;
			return 1;
		}
		else if (result == -2)
		{
			cerr << "Error reading file" << endl;
			return 1;
		}
	}

	// Read input numbers in first processes and sort them
	if (rank == 0 || rank == 1 || rank == 2 || rank == 3)
	{
		// Read assigned numbers
		int num1 = read_num(file_name, 2*rank);
		int num2 = read_num(file_name, 2*rank+1);

		if (num1 == -1)
		{
			cerr << "Could not open file" << endl;
			return 1;
		}
		else if (num1 == -2)
		{
			cerr << "Error reading file" << endl;
			return 1;
		} 

		if (num2 == -1)
		{
			cerr << "Could not open file" << endl;
			return 1;
		}
		else if (num2 == -2)
		{
			cerr << "Error reading file" << endl;
			return 1;
		}

		// Compare numbers
		int low, high;

		if (num1 < num2)
		{
			low = num1;
			high = num2;
		}
		else
		{
			low = num2;
			high = num1;
		}

		//cout << "Process: " << rank << ", " << "Low = " << send_map[rank][0] << " High = " << send_map[rank][1] << endl;
		//cout << "Process: " << rank << ", " << "IN1 = " << num1 << " IN2 = " << num2 << " | Low = " << low << " High = " << high << endl;

		// Send lower number to its next process in network && send bigger number to its next process in network
		MPI_Send(&low, 1, MPI_INT, send_map[rank][0], DEFAULT_TAG, MPI_COMM_WORLD);
		MPI_Send(&high, 1, MPI_INT, send_map[rank][1], DEFAULT_TAG, MPI_COMM_WORLD);
	}
	// Sort recieved numbers and send them to next processes
	else
	{
		//cout << "Process: " << rank << ", IN1 = " << recv_map[rank -4][0] << " IN2 = " << recv_map[rank-4][1] << " | " << "Low = " << send_map[rank][0] << " High = " << send_map[rank][1] << endl;

		// Recieve numbers
		int num1,num2;

		// rank - 4, because first four processes read values from input file 
		MPI_Recv(&num1, 1, MPI_INT, recv_map[rank - 4][0], MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		MPI_Recv(&num2, 1, MPI_INT, recv_map[rank - 4][1], MPI_ANY_TAG, MPI_COMM_WORLD, &status);

		// Compare numbers
		int low, high;

		if (num1 < num2)
		{
			low = num1;
			high = num2;
		}
		else
		{
			low = num2;
			high = num1;
		}

		//cout << "Process: " << rank << ", " << "IN1 = " << num1 << " IN2 = " << num2 << " | Low = " << low << " High = " << high << endl;

		// If next process is 0 than number is sorted and will be send to first process for final print
		MPI_Send(&low, 1, MPI_INT, send_map[rank][0], LOW_TAG, MPI_COMM_WORLD);
		MPI_Send(&high, 1, MPI_INT, send_map[rank][1], HIGH_TAG, MPI_COMM_WORLD);
	}

	// Collect sorted numbers in first process and print them
	if (rank == 0)
	{
		// Recieve numbers and make output string
		int numbers[input_size];
		string out_string = "";

		for (int i = 0; i < input_size; i++)
		{
			//cout << "Waiting on = " << sort_num_sender[i][0] << "|" <<  sort_num_sender[i][1] << endl; 	
			MPI_Recv(&(numbers[i]), 1, MPI_INT, sort_num_sender[i][0], sort_num_sender[i][1], MPI_COMM_WORLD, &status);
			//cout << "Got = " << numbers[i] << endl << endl;
			out_string.append(to_string(numbers[i]));
			out_string.append("\n");
		}

		// Print numbers in specified format
		cout << out_string;
	}

	// Stop processes and end program
	MPI_Finalize();
	return 0;	
}

// END oems.cpp
