#!/bin/bash

# test.sh
# Odd-Even Merge Sort, PRL project 1, 20.3.2022
# Author: Matěj Kudera, xkuder04, FIT
# Test script that runs implementation o algorithm Odd-Even Merge Sort.

# Compile source file
mpic++ --prefix /usr/local/share/OpenMPI -o oems oems.cpp

# Creation of input file with random numbers
dd status=none if=/dev/random bs=1 count=8 of=numbers

# Run compiled Odd-Even Merge Sort
mpirun --prefix /usr/local/share/OpenMPI -oversubscribe -np 19 oems

# Remove created files
rm -f oems numbers
